import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class FoodOrder {
    private JPanel root;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JButton checkOutButton;
    private JTextPane ordertextbox;
    private JLabel totalvalue;
    private int total=0;//合計値段
    String[] selectSetMenu={"Nothing","Miso Soup","Pickles","Raw Egg"};//セットメニュー品目
    int[] setMenuValue={0,78,108,68};//セットメニューの値段

    void order(String food,int value){

        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);//商品注文の確認

        if(confirmation==0) {//Yesを選択した場合
            String currentText = ordertextbox.getText();
            ordertextbox.setText(currentText+food+" "+value+"yen\n");
            total+=value;//選択した食品を注文リストに追加

            int selectSetMenuValue=JOptionPane.showOptionDialog(null,
                    "Would you like side menu?",
                    "Select side menu",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    selectSetMenu,
                    selectSetMenu[0]
            );//セットメニューの追加ダイアログ

            if(selectSetMenuValue!=0) {//Nothing以外を選択した場合
                currentText = ordertextbox.getText();
                total += setMenuValue[selectSetMenuValue];
                ordertextbox.setText(currentText + "  +option:" + selectSetMenu[selectSetMenuValue] + " " + setMenuValue[selectSetMenuValue] + "yen\n");//サイドメニューを注文リストへ追加
            }

            totalvalue.setText("Total   "+total+"yen");//合計金額の更新
            JOptionPane.showMessageDialog(null,
                    "Thank you for Ordering "+food+"! It will be served as soon as possible.");

        }

    }

    public FoodOrder() {
        //ordertextbox.setText("No Items");
        totalvalue.setText("Total   "+total+"yen");

        button1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
               order("Beef Bowl",352);
            }
        });
        button1.setIcon(new ImageIcon(this.getClass().getResource("牛丼.jpg")
        ));



        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakiniku Beef Barbecue Bowl",548);
            }
        });
        button2.setIcon(new ImageIcon(this.getClass().getResource("牛焼き肉丼.jpg")
        ));

        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Beef Bowl with Toppings of Green Onions and Raw Egg",460);

            }
        });
        button3.setIcon(new ImageIcon(this.getClass().getResource("ネギ玉牛丼.jpg")
        ));

        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Beef Simmered with Egg with Raw Egg Bowl",602);

            }
        });
        button4.setIcon(new ImageIcon(this.getClass().getResource("月見牛とじ丼.jpg")
        ));

        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Fried Chicken Meal",458);

            }
        });
        button5.setIcon(new ImageIcon(this.getClass().getResource("からあげ丼.jpg")
        ));

        button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Broiled Eel and Rice Box",788);

            }
        });
        button6.setIcon(new ImageIcon(this.getClass().getResource("うな重.jpg")
        ));

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to chekout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);//チェックアウトの確認ダイアログ

                if(confirmation==0){
                    JOptionPane.showMessageDialog(null,
                            "Your payment: "+total+"yen.\n"+"Thank you.");//精査ダイアログ

                    //合計金額の初期化
                    total=0;
                    totalvalue.setText("Total   "+total+"yen");

                    //ordertextbox.setText("No Items");
                    ordertextbox.setText("");//注文リストの初期化
                }

            }
        });

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrder");
        frame.setContentPane(new FoodOrder().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
